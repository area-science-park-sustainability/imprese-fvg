#Authors: Fabio Morea, Leyla Vesnic @ Area Science Park
#Description: python scripts to clean and prepare data on regional companies.

#L'assegnazione dell'ateco univoco si basa sulla tipologia degli ateco dichiarati dall'impresa: (I) prevalente, (P) primario e (S) secondario. Laddove presente, il codice univoco corrisponde al codice prevalente, mentre, nei casi in cui il prevalente non è evidente, viene scelto il codice primario (il primo se presenti più di uno).

#Setup
import sys
import os
from pathlib import Path
import datetime
import pandas as pd
#import numpy as np
pd.options.display.max_columns = None

#Path
current_path = Path(os.getcwd())
data_subdir = "data"
data_path = current_path.parent / data_subdir
data_dir = str(data_path)
listafile = os.listdir(data_path)
filename = 'imprese_codici.csv'
assert filename in listafile, "Errore: non trovo il file dati"

#crea dizionario con i nomi di colonna originali e corretti
df = pd.read_csv(data_dir + '\\' + filename, sep = '|', dtype='str') 

#elimina le righe senza codici ateco (ateco_tipo = "")
df= df[ df['ateco_tipo'].str.len() >0  ]

#crea info sulle priorità
df['priorita'] = df['ateco_tipo']
df = df.replace({"priorita": { 'I':1, 'P':2, 'S':3} })
df.sort_values(['cf', 'priorita' ,'loc_n'  ], inplace = True)

def ateco_univoco(dataframe, cf_impresa):
    df_ateco_impresa = dataframe[ dataframe.cf == cf_impresa]
    lista_ateco = []
    for index,row in  df_ateco_impresa.iterrows():
        #print(index, row.ateco_tipo, row.loc_n, row.ateco )
        temp = str(row.ateco_tipo) + str(row.ateco) 
        lista_ateco.append(temp)
    result = lista_ateco[0] if len(lista_ateco)>0 else "n.d."
    return result#, lista_ateco

ateco_univoco(df, '00000470310') #esempio di impresa senza codici, inattiva

assert ateco_univoco(df, '00000470310') == 'n.d.'

ateco_univoco(df, '00030280267') 

df2 =  df.drop_duplicates(subset='cf')
df2 = df2.assign(ateco_uni = "") 

from tqdm import tqdm #tiene conto dei tempi e crea una barra di avanzamento

for index,row in tqdm(df2.iterrows()):
    #print('\n\n\n ***', index, row.cf, au)
    df2.loc[index, 'ateco_uni'] = ateco_univoco(df, row.cf)

df2.to_csv(data_dir + '\\i_ateco_univoco.csv', sep='|', encoding='utf-8-sig', index=False)


