# Setup
import sys
import os
from pathlib import Path
import datetime
import pandas as pd
pd.options.display.max_columns = None

# path
current_path = Path(os.getcwd())
data_subdir = "data"
data_path = current_path.parent / data_subdir
data_dir = str(data_path)

repo_subdir = "repository"
repo_path = current_path.parent / repo_subdir
repo_dir = str(repo_path)
 
repo_dir, data_dir

# FRIULI anagrafica
filename = data_dir +'\\imprese_anagrafica.csv'
col_names = pd.read_csv(filename, nrows=0,sep = '|').columns
dtypes={'id_localiz': int, 'id_impresa': int,  'piva':str}
types_dict = {'id_localiz': int, 'id_impresa': int}
types_dict.update({col: str for col in col_names if col not in types_dict})
parse_dates = [d for d in col_names if d.startswith('data')]

df_anagrafica = pd.read_csv (  filename, sep = '|', dtype=types_dict, parse_dates=parse_dates)
df_anagrafica.info()

# FRIULI codici attività
filename = data_dir +'\\imprese_codici.csv'
dtypes={'id_localiz': int, 'id_impresa': int, 'loc_n':int }
cols_to_use = [ 'mm_aaaa', 'fonte', 'id_localiz', 'id_impresa' ,'loc_n', 'ateco_tipo', 'ateco', 'ateco_desc'] 
df_codici = pd.read_csv (  filename, sep = '|', usecols = cols_to_use , dtype = dtypes)
df_codici.info()

# Creazione dei "dizionari" csv

# creo file csv per tabela imprese - localizzazioni
file_risultati  = repo_dir + '\\' + 'id_imp_loc.csv'
cols_to_use = ['id_impresa', 'id_localiz']
df = df_anagrafica[ cols_to_use ]
df.to_csv(file_risultati, sep ='|',encoding='utf-8-sig', index=False)

# Crea dizionario descrizioni ateco
file_risultati  = repo_dir + '\\' +  'd_ateco.csv'
cols_to_use = ['ateco', 'ateco_desc']
df = df_codici[cols_to_use]
df.drop_duplicates(inplace = True)
df.sort_values(by='ateco', ascending=True, inplace=True)
df.to_csv(file_risultati, sep ='|',encoding='utf-8-sig', index=False)

# df_codici.drop(columns = 'ateco_desc', axis = 1) 
   
# Crea dizionario ng
file_risultati  = repo_dir + '\\' + 'd_ng.csv'
cols_to_use = ['tipo_impresa', 'ng2', 'ng_esteso']
df = df_anagrafica[ cols_to_use ]
df['ng_esteso'] =  df['ng_esteso'].str[5:]
df.drop_duplicates(inplace = True)
df.sort_values(by=['tipo_impresa','ng2'], ascending=False, inplace=True)

df.to_csv(file_risultati, sep ='|',encoding='utf-8-sig', index=False)
#df_anagrafica.drop(columns = 'ng_esteso', axis = 1)   

# Crea lista descrizioni attività

# NB c'è qualche problema di caratteri impropri / taglia le descrizioni 
cols_to_use = [ 'id_impresa', 'descrizione_attivita']
file_risultati  = repo_dir + '\\' + 't_attivita.csv'

df = df_anagrafica[cols_to_use]

df.drop_duplicates( inplace = True)

df.sort_values(by='id_impresa', ascending=True, inplace=True)

df.to_csv(file_risultati,sep ='|',encoding='utf-8-sig',  index=False)

#crea il file delle localizzazioni
file_risultati  = repo_dir + '\\' + 't_localizz.csv'
cols_localiz =      [
                    'fonte',   'id_localiz', 'id_impresa', 'denominazione',
                    'tipo_localizzazione',                    
                    'data_apert_ul', 
                    'comune', 'indirizzo',
                    'tipo_sedeul'
                    ]
df = df_anagrafica[cols_localiz]
#df.fillna('', inplace=True)
# tipo_localizzazione: 
# valori possibili: 
# {'ST - SEDE TRASFERITA', 'SS - SEDE SECONDARIA', 'UL - UNITÀ LOCALE', 'SE - SEDE PRINCIPALE'}
print(set (df['tipo_localizzazione']))
df['tipo_localizzazione'] = df['tipo_localizzazione'].str[0:2]

# le localizzazioni possono avere più di un "tipo" 
# https://www.registroimprese.it/sede-legale-e-unita-locali

df.sort_values(by='id_localiz', ascending=True, inplace=True)
df['prov_localiz'] = df['comune'].str[-3:]
df['comune'] = df['comune'].str[:-5]

cols_order = ['fonte',  'id_localiz', 'id_impresa', 'denominazione', 'tipo_localizzazione',  'data_apert_ul', 'prov_localiz', 'comune' , 'indirizzo', 'tipo_sedeul']

df[cols_order].to_csv(file_risultati, sep ='|',encoding='utf-8-sig', index=False)
df.columns

# crea file csv con pseudonimizzazione dei CF <> id_impresa
file_risultati  = repo_dir + '\\' + 'pseudo_cf_id_impresa.csv'

cols_imp =          ['fonte',  'id_impresa', 'cf']   
df = df_anagrafica[cols_imp]

df.drop_duplicates(subset = 'id_impresa', inplace = True) 
df.sort_values(by='id_impresa', ascending=True, inplace=True)

df.to_csv(file_risultati, sep ='|',encoding='utf-8-sig', index=False )

file_risultati  = repo_dir + '\\' + 't_imprese.csv'

cols_imp =          [
                    'fonte',  'mm_aaaa', 'id_impresa',  
'cf', 'piva', 'denominazione',
                    'prov', 'reg_imp_n',  'sede_ul', 'n-albo_art',
                    'reg_imp_sez', 'ng2',   
                    'stato_impresa',
                    'data_cost','data_isc_ri', 'data_isc_rd', 'data_isc_aa',
                    'data_canc', 'data_ini_at', 
                    'data_cess_att',  'data_fall', 'data_liquid',
                    'addetti_aaaa', 'addetti_indip', 'addetti_dip', 
                    'capitale', 'capitale_valuta',
                    'imp_sedi_ee', 'imp_eefvg', 'imp_pmi', 'imp_startup',
                    'imp_femmilile', 'imp_giovanile', 'imp_straniera' , 
                    
                    ]   

df = df_anagrafica[cols_imp]

## attenzione! "drop duplicates" non va bene perchè seleziona la provincia "a caso"
#  è necessario scegliere la provincia della sede legale con n_sede = 0
df.sort_values(['cf', 'sede_ul' ], inplace = True)
df['cf_precedente'] = df['cf'].shift(1)
df['test'] = (df['cf'] != df['cf_precedente'])
df = df[ df['test'] ]

#df.drop_duplicates(subset = 'id_impresa', inplace = True) 

df.sort_values(by='id_impresa', ascending=True, inplace=True)

cols_order = ['fonte', 'mm_aaaa','id_impresa', 'denominazione', 'cf', 'piva', 'prov', 'reg_imp_n',
        'sede_ul', 'n-albo_art', 'reg_imp_sez', 'ng2', 'stato_impresa',
       'data_cost', 'data_isc_ri', 'data_isc_rd', 'data_isc_aa', 'data_canc',
       'data_ini_at', 'data_cess_att',  'data_fall',
       'data_liquid', 'addetti_aaaa', 'addetti_indip', 'addetti_dip',
       'capitale', 'capitale_valuta', 'imp_sedi_ee', 'imp_eefvg', 'imp_pmi',
       'imp_startup', 'imp_femmilile', 'imp_giovanile', 'imp_straniera']


df[cols_order].to_csv(file_risultati, sep ='|',encoding='utf-8-sig', index=False )

# # salvataggio file csv imprese e codici
file_risultati  = repo_dir + '\\' + 't_imprese_dp.csv'

cols_imp_dp =       [
                    'fonte', 'mm_aaaa', 'id_impresa', 
                    'cf','denominazione'
                    ]
df = df_anagrafica[cols_imp_dp]


df.drop_duplicates(subset = 'id_impresa', inplace = True) 

df.sort_values(by='id_impresa', ascending=True, inplace=True)
cols_order = ['fonte', 'mm_aaaa',  'id_impresa', 'cf', 'denominazione']
df[cols_order].to_csv(file_risultati, sep ='|',encoding='utf-8-sig', index=False)

file_risultati  = repo_dir + '\\' + 't_codici.csv'
cols_imp_dp = ['fonte', 'mm_aaaa', 'id_localiz', 'loc_n', 'ateco_tipo', 'ateco']
df = df_codici[cols_imp_dp]
df.sort_values(by='id_localiz', ascending=True, inplace=True)
df.to_csv(file_risultati, sep ='|',encoding='utf-8-sig', index=False)

print('end')


