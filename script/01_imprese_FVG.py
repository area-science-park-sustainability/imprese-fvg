# Setup
import sys
import os
from pathlib import Path
import datetime
import pandas as pd
import numpy as np
pd.options.display.max_columns = None

# path
current_path = Path(os.getcwd())
print(current_path)
data_subdir = "data"
data_path = current_path / data_subdir
data_dir = str(data_path)
listafile = os.listdir(data_path)
listafile = list(filter(lambda x: 'imprese_fvg_' in x, listafile))  
assert len(listafile) >= 1, f"Errore: non trovo i file dati nella cartella {data_dir}"

# specify the name of the correct the file to be import
file_da_elaborare = '11_2022'   # mese da modificare ad ogni aggiornamento
excel_file  = data_dir + '\\imprese_fvg_' + file_da_elaborare + '.xlsx'
print(f'Carico i dati presenti nel file {excel_file}')

xl = pd.ExcelFile(  excel_file, engine="openpyxl")
xl_sheets = xl.sheet_names  # see all sheet names
assert xl_sheets == ['FRIULI Anagrafica', 'FRIULI codice attività']

# FRIULI ANAGRAFICA

df_anagrafica = xl.parse(   'FRIULI Anagrafica', 
                            header = 0, 
                            dtype=str,
                            keep_default_na=False)

#crea dizionario con i nomi di colonna originali e corretti
cols_df = pd.read_excel('cols_dict.xlsx', sheet_name='anagrafica') 
l1 = cols_df['nomi_colonne_originali']
l2 = cols_df['nomi_colonne_corretti']
cols_dic = dict(zip(l1,l2))
df_anagrafica.rename(columns=cols_dic, inplace=True)

# aggiungo campi con fonte, riferimento temporale e numero sede
df_anagrafica['fonte'] = 'I'
df_anagrafica['mm_aaaa'] = file_da_elaborare
df_anagrafica['n_sede'] = df_anagrafica['sede_ul'].str[3:] 
df_anagrafica.loc[ df_anagrafica['n_sede'] == 'E', 'n_sede'] = '0'
# alcune UL hanno separatore delle migliaia!
df_anagrafica['n_sede'] = df_anagrafica['n_sede'].str.replace('.' ,'', regex = True) 

# TODO: aggiungere controlli sui dati mancanti: sostituire con NaN o con "" 
dim = df_anagrafica.shape #numero di righe e colonne del dataset
print(f'dimensioni del dataset (righe,colonne) = {dim}\n')
print(df_anagrafica.info())

# strip special characters
# chars_to_strip = '\\n\\t\\r|#'
cols_to_strip = [ 'denominazione',  'descrizione_attivita', 'indirizzo']
for ccc in cols_to_strip:
        if isinstance(ccc, str): 
                df_anagrafica[ccc] = df_anagrafica[ccc].str.replace(  '\\n',     ' ', regex = True)
                df_anagrafica[ccc] = df_anagrafica[ccc].str.replace(  '\\t',     ' ', regex = True)
                df_anagrafica[ccc] = df_anagrafica[ccc].str.replace(  '\\r',     ' ', regex = True)
                df_anagrafica[ccc] = df_anagrafica[ccc].str.replace(  '"' ,      ' ', regex = True)
                df_anagrafica[ccc] = df_anagrafica[ccc].str.replace(  "'" ,      ' ', regex = True)
                df_anagrafica[ccc] = df_anagrafica[ccc].str.replace(  '|' ,      ' ', regex = True)
                df_anagrafica[ccc] = df_anagrafica[ccc].str.replace(  '_x000D_', ' ', regex = True)
                df_anagrafica[ccc] = df_anagrafica[ccc].str.replace(  '' ,      ' ', regex = True)
                df_anagrafica[ccc] = df_anagrafica[ccc].str.replace(  '#' ,      ' ', regex = True)
                df_anagrafica[ccc] = df_anagrafica[ccc].str.replace(  '' ,      ' ', regex = True)
                df_anagrafica[ccc] = df_anagrafica[ccc].str.replace(  '#' ,      ' ', regex = True)
                df_anagrafica[ccc] = df_anagrafica[ccc].str.replace(  '*' ,      ' ', regex = True)
                df_anagrafica[ccc] = df_anagrafica[ccc].str.replace(  '-' ,      ' ', regex = True)
                df_anagrafica[ccc] = df_anagrafica[ccc].str.strip()
                print(ccc, " ok")

df_anagrafica['reg_imp_n'] = df_anagrafica['reg_imp_n'].str.replace(  '(', '', regex = True)
df_anagrafica['reg_imp_n'] = df_anagrafica['reg_imp_n'].str.replace(  ')', '', regex = True)

df_anagrafica= df_anagrafica.loc[df_anagrafica['data_cess_att'].isin([''])]

# Le date nel file originale hanno alcuni problemi: (1) sottrarre 3000 all'anno, 
# (2) gestire correttamente errori e "nan", (3)eliminare date antecedenti al 1800 o successive al 2099

def anno_corretto(dstring:str) -> str:
    num =0
    dstring = str(dstring)[:10]
    if len(dstring) < 4:
        result = "NaT"
    else:
        try:
            num = int(dstring[0:2])
            if num >= 48 and num <= 51: #anni fra il 1800 e il 2100
                num = num - 30
                result = str(num) + dstring[2:]
            else:
                result = "NaT"
        except:
            result = "NaT"
    return result   
assert anno_corretto('x')   == 'NaT'
assert anno_corretto('1799-03-01') == 'NaT'
assert anno_corretto('2100-03-01') == 'NaT'
assert anno_corretto('4987-03-01') == '1987-03-01'
assert anno_corretto('5021-03-01') == '2021-03-01'
pass

cols_date = ['data_isc_ri', 'data_isc_aa', 'data_liquid','data_cess_att',  'data_fall', 'data_apert_ul', 'data_fine_aa']
#'data_isc_rd'
#'data_ini_at'
#'data_cost'

# i tre campi data in formato commento non vengono gestiti dalla funzione "anno corretto" in modo da lasciare il +3000 da poter gesitre in excel

#cols_date = [d for d in df_anagrafica.columns if d.startswith('data')]
print(cols_date)
for col in cols_date:
    datestring3000 = df_anagrafica[col].tolist()
    #print(datestring3000)
    datestring = [anno_corretto(item) for item in datestring3000]
    df_anagrafica[col] = pd.to_datetime(datestring)#, errors = 'coerce')
    #print(df_anagrafica[col])

# id localiz e id impresa
#creo key_clf che servirà per connettere id_localiz al dataset dei codici
df_anagrafica['key_cfl'] = df_anagrafica['cf'] + '_'  + df_anagrafica['n_sede'].apply(str)

# creo colonna temporanea anni per ordinare gli indici
df_anagrafica['min_date']   = pd.to_datetime( df_anagrafica[cols_date].min(axis=1) )
df_anagrafica['today']      = pd.to_datetime( pd.Timestamp.today().strftime('%Y-%m-%d') ) 
df_anagrafica['anni'] =  (df_anagrafica['today']  - df_anagrafica['min_date']) / np.timedelta64(1, 'Y')
df_anagrafica.drop(columns='today')

df_anagrafica.sort_values(by = ['anni', 'key_cfl'], ascending=False, inplace = True)

# creo id_localiz (dall'indice, mi aspetto 205646)
df_anagrafica.reset_index(inplace=True)
df_anagrafica.reset_index(inplace=True)
df_anagrafica['id_localiz'] = df_anagrafica['index'] + 1

df_anagrafica.sort_values(by = 'index', inplace = True)

#creo id impresa univoco collegato a CF
df_cf_univoco = df_anagrafica[['cf', 'denominazione']]
df_cf_univoco.drop_duplicates(subset='cf', inplace = True)
df_cf_univoco.reset_index(inplace = True)
df_cf_univoco.reset_index(inplace = True)
df_cf_univoco['id_impresa'] = df_cf_univoco['level_0'] +1
df_cf_univoco.columns

df_cf_univoco.drop(columns=['level_0', 'index'], inplace = True)
cols_order = ['id_impresa', 'cf' ]
df_cf_univoco = df_cf_univoco[cols_order]

df_anagrafica = df_anagrafica.merge(df_cf_univoco, on = 'cf', how = 'inner') 

# Controlli su unità locali senza sedi di riferimento - controllo duplicati - controllo date
# funzione per creare nuova colonna dicotomica
def f(row):
    if row['sede_ul'] == 'SEDE':
        val = 's'
    else:
        val = 'u'
    return val

df_anagrafica['SEDE_UL_num'] = df_anagrafica.apply(f, axis = 1)

# tabella pivot - conteggio sedi/unità locali per cf
pivot_test = df_anagrafica.pivot_table(index = "cf",
                                values = "sede_ul",
                                columns='SEDE_UL_num',
                                aggfunc = lambda x : x.count(),
                                margins = True,
                                fill_value=0)

#lista cf senza sede
df_test = pivot_test
cf_no_sede = df_test[ df_test['s']==0 ]
cf_no_sede = cf_no_sede.reset_index()
cf_no_sede = cf_no_sede['cf'].to_list()

df_anagrafica = df_anagrafica[~df_anagrafica['cf'].isin(cf_no_sede)]

df_anagrafica.groupby('stato_impresa').count()

# Gestione dei duplicati

#df_anagrafica['id_unitàlocale'] = df_anagrafica['cf'] + df_anagrafica['sede_ul'] + df_anagrafica['comune']
#df_anag_dup = df_anagrafica[df_anagrafica[['id_unitàlocale']].duplicated()]
#df_anag_dup = df_anag_dup[~df_anag_dup['stato_impresa'].isin(['ATTIVA'])]
#id_dup = df_anag_dup.index.values.tolist()
#df_anagrafica.drop(id_dup, axis=0, inplace=True)
#df_anag_dup.tail(20)

#df_2 = df_anagrafica[df_anagrafica[['id_unitàlocale']].duplicated(keep='last')]
#id_dup_2 = df_2.index.values.tolist()
#df_anagrafica.drop(id_dup_2, axis=0, inplace=True)

df_dup = df_anagrafica[df_anagrafica.duplicated(subset=['cf','sede_ul'], keep=False)]

df_dup = df_dup[df_dup['prov'] != df_dup['prov_camera_commercio']]
df_dup[df_dup['cf'] == '07381630966']

id_df_dup = df_dup.index.values.tolist()
df_anagrafica.drop(id_df_dup, axis=0, inplace=True)

df_dup_test_final = df_anagrafica[df_anagrafica.duplicated(subset=['cf','sede_ul'], keep=False)]
df_dup_test_final

# verifico che fra i duplicati rimasti non ci siano le stesse ul nella stessa provincia
df_dup_test_final_2 = df_dup_test_final[df_dup_test_final.duplicated(subset=['cf','sede_ul','prov'], keep=False)]
df_dup_test_final_2

#f_3 = df_anagrafica[df_anagrafica.duplicated(subset=['cf','sede_ul'], keep=False)]
#df_3 = df_3[~df_3['stato_impresa'].isin(['ATTIVA'])]
#id_df_3 = df_3.index.values.tolist()
#df_anagrafica.drop(id_df_3, axis=0, inplace=True)

# df_4 = df_anagrafica[df_anagrafica.duplicated(subset=['cf','sede_ul'], keep='last')]
# id_df_4 = df_4.index.values.tolist()
# df_anagrafica.drop(id_df_4, axis=0, inplace=True)

#df_anagrafica = df_anagrafica.drop('id_unitàlocale', axis=1)

df_anagrafica.groupby('stato_impresa').count()

df_anagrafica.shape[0]

# FRIULI codici attività

df_codici = xl.parse('FRIULI codice attività',  
                    header = 0,
                    dtype=str,
                    keep_default_na=False) 

#crea dizionario con i nomi di colonna originali e corretti
cols_df = pd.read_excel('cols_dict.xlsx', sheet_name='codici') 
l1 = cols_df['nomi_colonne_originali']
l2 = cols_df['nomi_colonne_corretti']
cols_dic = dict(zip(l1,l2))

df_codici.rename(columns=cols_dic, inplace=True)

df_codici['fonte'] = 'I'
df_codici['mm_aaaa'] = file_da_elaborare

# creo indice key_clf anche nel df_codici per poter collegare id_localiz
df_codici['key_cfl'] = df_codici['cf'] + '_'  + df_codici['loc_n']

df_temp = df_anagrafica[['key_cfl',  'id_impresa', 'id_localiz']]

# aggiungo id_localiz in df_codici
df_codici = df_codici.merge(df_temp, on = 'key_cfl', how = 'inner') 

# filtriamo anche il foglio codici eliminando i cf senza sede
df_codici = df_codici[~df_codici['cf'].isin(cf_no_sede)]

df_cod_dup = df_codici[df_codici.duplicated(subset=['cf','prov', 'rea', 'loc_n', 'ateco_tipo','ateco','ateco_desc'], keep=False)]

# Salvataggio di file .csv per diverse destinazioni

# File .csv per Repository

# Dovendo fondere i campi relativi a "tipo_sedeul_n", creo una copia del dataframe per poter operare la fusione sulla copia e salvarla nella versione "Repository"

df_anagrafica_repo = df_anagrafica.copy()
# unione dei campi tipo_sedeul
df_anagrafica_repo['tipo_sedeul'] =  df_anagrafica_repo.tipo_sedeul_1+df_anagrafica_repo.tipo_sedeul_2+df_anagrafica_repo.tipo_sedeul_3+df_anagrafica_repo.tipo_sedeul_4+df_anagrafica_repo.tipo_sedeul_5
df_anagrafica_repo.drop(columns=['tipo_sedeul_1', 'tipo_sedeul_2', 'tipo_sedeul_3', 'tipo_sedeul_4',
       'tipo_sedeul_5'], inplace=True)

# Salva il file ANAGRAFICA in versione "repository": 
file_risultati = data_dir + '\\' + 'imprese_anagrafica.csv'
cols_df = pd.read_excel('cols_dict.xlsx', sheet_name='anagrafica',
            usecols = ['nomi_colonne_export','ordine_repo']).dropna() 
cols_df.sort_values('ordine_repo', inplace = True)
cols_to_use = list(cols_df['nomi_colonne_export'])
df_anagrafica_repo[cols_to_use].to_csv(  file_risultati, 
                                    sep ='|',   
                                    encoding='utf-8-sig', 
                                    index=False)

# Salva il file CODICI in versione "repository": 
file_risultati = data_dir + '\\' + 'imprese_codici.csv'
cols_df = pd.read_excel('cols_dict.xlsx', sheet_name='codici',
            usecols = ['nomi_colonne_corretti','ordine_repo']).dropna() 
cols_df.sort_values('ordine_repo', inplace = True)
cols_to_use = list(cols_df['nomi_colonne_corretti'])
df_codici[cols_to_use].to_csv(      file_risultati, 
                                    sep ='|',   
                                    encoding='utf-8-sig', 
                                    index=False)

# File. csv per Innvoation Intelligence (unità locali anche extra FVG)

# Salva il file ANAGRAFICA file in versione "innovation intelligence": 
file_risultati = data_dir + '\\' + 'iifvg_anagrafica.csv'
cols_df = pd.read_excel('cols_dict.xlsx', sheet_name='anagrafica',
            usecols = ['nomi_colonne_corretti','ordine_iifvg']).dropna() 
cols_df.sort_values('ordine_iifvg', inplace = True)
cols_to_use = list(cols_df['nomi_colonne_corretti'])
df_anagrafica[cols_to_use].to_csv(  file_risultati, 
                                    sep ='|',   
                                    encoding='utf-8-sig', 
                                    index=False)

# Salva il file CODICI in versione "innovation intelligence": 
file_risultati = data_dir + '\\' + 'iifvg_codici.csv'
cols_df = pd.read_excel('cols_dict.xlsx', sheet_name='codici',
            usecols = ['nomi_colonne_corretti','ordine_iifvg']).dropna() 
cols_df.sort_values('ordine_iifvg', inplace = True)
cols_to_use = list(cols_df['nomi_colonne_corretti'])
df_codici[cols_to_use].to_csv(      file_risultati, 
                                    sep ='|',   
                                    encoding='utf-8-sig', 
                                    index=False)

# File .csv per Innovation Intelligence (solamente unità locali FVG)

local_sede = ["SEDE"]
prov_FVG = ["GO", "PN", "UD", "TS"]
#filtro ed elimino le unità locali extra FVG presenti nell'anagrafica di Insiel
unità_locali_extraFVG_filter = ~df_anagrafica["sede_ul"].isin(local_sede) & ~df_anagrafica["prov"].isin(prov_FVG)
#df_anagrafica[unità_locali_extraFVG_filter].drop()
df_anagrafica = df_anagrafica[~unità_locali_extraFVG_filter]

# Salva il file ANAGRAFICA file in versione "innovation intelligence" senza UL extra FVG
file_risultati = data_dir + '\\' + 'iifvg_anagrafica_filtrato.csv'
cols_df = pd.read_excel('cols_dict.xlsx', sheet_name='anagrafica',
            usecols = ['nomi_colonne_corretti','ordine_iifvg']).dropna() 
cols_df.sort_values('ordine_iifvg', inplace = True)
cols_to_use = list(cols_df['nomi_colonne_corretti'])
df_anagrafica[cols_to_use].to_csv(  file_risultati, 
                                    sep ='|',   
                                    encoding='utf-8-sig', 
                                    index=False)

local_sede = ["0"]
prov_FVG = ["GO", "PN", "UD", "TS"]
#filtro ed elimino le unità locali extra FVG presenti nel folgio codici dell'anagrafica Insiel
unità_locali_extraFVG_filter = ~df_codici["loc_n"].isin(local_sede) & ~df_codici["prov"].isin(prov_FVG)
#print(~unità_locali_extraFVG_filter)
#df_codici[unità_locali_extraFVG_filter].drop()
df_codici = df_codici[~unità_locali_extraFVG_filter]

# Salva il file CODICI in versione "innovation intelligence" senza UL extra FVG
file_risultati = data_dir + '\\' + 'iifvg_codici_filtrato.csv'
cols_df = pd.read_excel('cols_dict.xlsx', sheet_name='codici',
            usecols = ['nomi_colonne_corretti','ordine_iifvg']).dropna() 
cols_df.sort_values('ordine_iifvg', inplace = True)
cols_to_use = list(cols_df['nomi_colonne_corretti'])
df_codici[cols_to_use].to_csv(      file_risultati, 
                                    sep ='|',   
                                    encoding='utf-8-sig', 
                                    index=False)

# File .csv per Innovation Intelligence (SOLO unità locali FVG e SOLO società NON di capitale) - per integrazione nella piattaforma di I2

tipo_società = ["SOCIETA' DI CAPITALE"]
#filtro ed elimino le società di capitale
anagrafica_soc_capitale = df_anagrafica["tipo_impresa"].isin(tipo_società)
df_anagrafica = df_anagrafica[~anagrafica_soc_capitale]

# Salva il file ANAGRAFICA in versione "innovation intelligence" per integrazione dati
file_risultati = data_dir + '\\' + 'iifvg_anagrafica_filtrato_noncap.csv'
cols_df = pd.read_excel('cols_dict.xlsx', sheet_name='anagrafica',
            usecols = ['nomi_colonne_corretti','ordine_iifvg']).dropna() 
cols_df.sort_values('ordine_iifvg', inplace = True)
cols_to_use = list(cols_df['nomi_colonne_corretti'])
df_anagrafica[cols_to_use].to_csv(  file_risultati, 
                                    sep ='|',   
                                    encoding='utf-8-sig', 
                                    index=False)

# filtro del foglio CODICI in base ai cf di df_anagrafica (ultima versione)
filtro_df_anagrafica = df_anagrafica[['cf']].copy()
df_codici = df_codici[df_codici['cf'].isin(filtro_df_anagrafica['cf'].values)]

# Salva il file CODICI in versione "innovation intelligence" per integrazione dati
file_risultati = data_dir + '\\' + 'iifvg_codici_filtrato_noncap.csv'
cols_df = pd.read_excel('cols_dict.xlsx', sheet_name='codici',
            usecols = ['nomi_colonne_corretti','ordine_iifvg']).dropna() 
cols_df.sort_values('ordine_iifvg', inplace = True)
cols_to_use = list(cols_df['nomi_colonne_corretti'])
df_codici[cols_to_use].to_csv(      file_risultati, 
                                    sep ='|',   
                                    encoding='utf-8-sig', 
                                    index=False)


